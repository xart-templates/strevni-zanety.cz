jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	$('.menu_main').on('click',function(){
		$('body').toggleClass('menu_main-on');
	});

	$('.box_centres').each(function(){
		var i = $('li',this).length;
		if(i>1){
			$('ul',this).cycle({
				slides: 'li',
				fx: 'scrollHorz',
				speed: 200,
				timeout: 0,
				allowWrap: false,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
			});
		}
	});

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

});